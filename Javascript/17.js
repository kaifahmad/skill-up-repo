// Template litrales


const firstName = 'Mohd';
const job = 'teacher';
const birthYear = 1991;
const year = 2037;

const jonas = "I'm " + firstName + ", a " + (year - birthYear) + " year old " + job + "!"
console.log(jonas);


//other way of cantactenate
// by using $ symbol
// easier way 
// Template litrales
const jonasNew = `I'm ${firstName}, a ${year-birthYear} year old ${job}!`
console.log(jonasNew);

// console.log(`Just a regular string..`)
//back-tick  (``)


console.log('String with \n\
multiple\n\
lines')

// template litrals 
console.log(`String 
multiple
lines`)
