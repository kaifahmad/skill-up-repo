
// Let keyword are use to declare the variable 
// it is use to change the value of the variable in future cases 

let age = 30;
age = 31;
console.log(age )

//const keyword is also use to decklare the variable but once the value is assign it does change further likle in let keyword
const num = 2.14;
// num = 3;
console.log(num)
//  error 

//prefer o use const over let , use let only wghen are sure in future the value gonna change 


// var keyword is use the main difference is that in var allows the user to create the variable with the same name is it declare once and not giving error while let keywords gives the erroor .
var job = 'programmer'
job = 'teacher'
console.log(job)
var job = 'carpenter'
console.log(job)

