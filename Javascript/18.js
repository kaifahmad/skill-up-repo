// if-else statements

const age = 15;
if (age >= 18) {
    console.log('Sarah can start driving license');
}else{
    const yearLeft = 18 - age;
    console.log(`Sarah is too young. Wait another ${yearLeft} years`)
}

const birthYear = 2021;
let country;
if (birthYear<= 2000) {
     country = 20;
} 
else{
    country = 21;
}
console.log(country)