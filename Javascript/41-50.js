// 41 Coding Challenge

// const calcTip = function (bill){
//     return bill >= 50 && bill <= 300 ? bill *0.15 :bill*0.20;
  
// }


// // console.log(calcTip(100));
// const bills = [125, 555, 44];
// const tips = [calcTip(bills[0]),calcTip(bills[1]),calcTip(bills[2])];

// console.log(bills);
// console.log(tips);

// const totals = [bills
// [0] + tips[0],bills
// [1] + tips[1],bills
// [2] + tips[2]];

// console.log(totals);





// 42 Javascript Tutorail
//Introductions to objects
'use strict';

// const jonasArray = [
//     'Jonas',
//     'Mathey',
//     2037-1991,
//     'student',
//     ['yash', 'vikas','raju']
// ]
// console.log(jonasArray);
// this is a array and there are array inside array we can only refers the element of the array by there order in which they are appers 
// or we can say by there indexing.

// the concepts of objects is been introduced so that we can create key-value pair where we can assing value to key and refrs then with there keys.
// object define inside object

// const amaan = {
//     firstName : 'Mohd',
//     lastName : 'Amaan',
//     age : 2037-1991,
//     job : "teacher",
//     friends : ["Yash","Vikas", "Shivam"]

// }

// console.log(amaan);


// in object the main difference ios that it does not matter the order of the elements when we retrive them while in array it matters the most .

// object used for unstructural data while in array it  is use for structural data





// 43 Javascript 
// Dot Notation And Bracket Notation

// it is use to retrived data or property  from the object

// const amaan = {
//     firstName : 'Mohd',
//     lastName : 'Amaan',
//     age : 2037-1991,
//     job : "teacher",
//     friends : ["Yash","Vikas", "Shivam"]

// }
// console.log(amaan.lastName);// Dot
// console.log(amaan['lastName']); // Bracket

// const nameKey = 'Name';
// console.log(amaan['first' + nameKey]);// bracket Notation


// console.log(amaan.'first' + nameKey);// dot NOtation -> shows error


// braket notation can ne use with expresiion while in dot we need to name the actula property of the object in oder to retrived data from the object 

// const interested = prompt('What do you want to know about ?Choose between firstNam , lastName , Age, job,friends');

// prompt function gives us the pop in the window 

// if (amaan[interested]){
//     console.log(amaan[interested]);
    
// }else{
//     console.log("This property does not exit")
// }

// adding property to the object
// amaan.location = "India";
// amaan['twitter'] = '@amaanmohd';
// console.log(amaan);


// // Challenge
// console.log(`${amaan.firstName} has ${amaan.friends.length} friends, and his best friend is called ${amaan.friends[0]}`);

// dot notation is higher in precedence in respect with bracket notation 

// also they are from left-to-right






// 44 Javascript Tutorail
// Object Methods


// const amaan = {
//     firstName : 'Mohd',
//     lastName : 'Amaan',
//     birthYear : 1991,
//     job : "teacher",

//     friends : ["Yash","Vikas", "Shivam"],
//     hasDriversLicense: true,

//     // calcAge: function(birthYear){
//     //     return 2037-birthYear;
//     // }

//     // calcAge: function(){
//     //     // console.log(this);
//     //     return 2037-this.birthYear;
//     // }

//     calcAge: function(){
//         this.age = 2037 - this.birthYear;
//         return this.age;
//     },
//     getSummary : function (){
//         return `${this.firstName} is a ${this.calcAge()}-year old ${this.job}, and he has ${this.hasDriversLicense ? 'a' : "no"} driver's license.`
//     }

// }

// // using this. keywords we can call the object property inside a object and a function is defined and can be use the propety without been passing the parameter.



// // function can also be a property of a object 

// console.log(amaan.calcAge());


// console.log(amaan.age);
// console.log(amaan.age);
// console.log(amaan.age);

// // Challenge 

// console.log(amaan.getSummary());








//45 Javascript Tutorail
// Coding Challenge

// const mark = {
//     fullName : 'Mark Miller',
//     mass : 78,
//     height : 1.69,

//     calcBMI: function(){
//         this.bmiMark = this.mass / (this.height ** 2);
//         return this.bmiMark;
//     }
// }

// const john = {
//     fullName : 'John Smith',
//     mass : 92,
//     height : 1.95,

//     calcBMI: function(){
//         this.bmiJohn = this.mass / (this.height ** 2);
//         return this.bmiJohn ;
//     }
// }



// mark.calcBMI();
// john.calcBMI();

// console.log(mark.bmiMark, john.bmiJohn);


// if (mark.bmiMark > john.bmiJohn){

//     console.log(`${mark.fullName} BMI (${mark.bmiMark} is higher than ${john.fullName} (${john.bmiJohn}))`);
 
//  }
//   else{
//     console.log(`${john.fullName} BMI (${john.bmiMark} is higher than ${mark.fullName} (${mark.bmiJohn}))`);
//  }







//46 Javascript Tutorial
// For Loop

// foe loop keeps running while the conditon is true 
// in javascript we createa varaible that is used to run in the loop

// for (let rep = 1; rep<=10; rep++){
//     console.log(`Lifting weights repetition ${rep}`);
// }

// // 47 Javascript Tutorail
// const jonas = [
//         'Jonas',
//         'Mathey',
//         2037-1991,
//         'student',
//         ['yash', 'vikas','raju']
        
//     ];

// const types = []

// for (let i = 0; i<jonas.length; i++){
//     console.log(jonas[i] );

//     // filling a empty array in one way
//     // types[i] = typeof jonas[i]
     
//     // filling a empty array in second way using push function
//     types.push(typeof jonas[i]);
// }

// console.log(types);


// // Example 
// const years = [1991, 2007, 1978, 2020];
// const ages = []

// for (let i = 0; i<years.length ; i++){
//     ages.push(2037 - years[i]);

// }
// console.log(ages);



// continue statements 
// in continue statments it only exits the current iteration of the loop and start executing next iteratrion
// continue;


// Break Statement
// in break statements it exits the entire iteration of the loop 
//break;




// 48 
// looping backwards and loop inside loop

// const jonas = [
//     'Jonas',
//     'Mathey',
//     2037-1991,
//     'student',
//     ['yash', 'vikas','raju'],
//     true
// ];


// for (let i = jonas.length-1;  i>=0; i--){
//     console.log(jonas[i] );
// }

// // Nested Loop


// for (let excercise = 0;excercise<4;excercise++){
//     console.log(`-----Starting ${excercise}`);


//   for (let i = 0 ; i<7 ; i++){
//     console.log(`lifting weight ${i}`);
//   }

// } 







// 49 Javascript Tutorail
// While loop
// let i = 0;
// while (i<=5){
//     console.log(`----While loop Starting ${i}`);
//     i++;
// }

// let dice = Math.trunc(Math.random() * 6 + 1);
// // console.log(dice)

// while (dice !==6){
//     console.log(`You rolled a ${dice}`);
//     dice = Math.trunc(Math.random()*6)+1;
// }


//Math.Trunc is a library which removes the decimal part of the number 
// Math.random is also a library which gives random  numberr 

// we use while  loop when we dont know how many timees the loop gonna iterate 


// when we kmow then we use for loop






//50 
//Coding Challenge


// const calcTip = function(bill){
//     return  bill>= 50 && bill <=300 ? bill * 0.15 : bill * .20;
// }
// const bills = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52];

// const tips = []
// const totals = []


// for (let i = 0; i<bills.length ; i++){
//     const tip = calcTip(bills[i]);
//     tips.push(tip);
//     totals.push(tip + bills[i]);

// }

// console.log(tips);
// console.log(totals);

