
// 21 Javascript
//there are 5 Falsy Values
/*
1.0
2. ''
3. undefined
4. null
5. NaN */


// console.log(Boolean(0));
// console.log(Boolean(undefined));
// console.log(Boolean('jonas'));
// console.log(Boolean(''));
// console.log(Boolean({}));


// const money =  1100;
// if (money) {
//     console.log("Dont spend it all!")
// }else{
//     console.log("You should get a job!")
// }










// 22 Javascript
// strict equality operator === , t does not do type coresion 
// const agee = 18 ;
// if (agee === 18) console.log("you just become an adult")
// gives true 

// loose equality operator == ,  does type coresion 
// const age = '18' ;
// if (age === 18) console.log("you just become an adult")
// gives false 
// while when we use  loose quality operatoe it wont gives false o/p


// Prompt Function- help us to get the value from the user 

// prompt("What is your favourite number")


// const favourite =Number( prompt("What is your favourite number"))
// console.log(favourite);
// console.log(typeof favourite);

// if (favourite === 23) {
//     console.log(`cool ${favourite} is an amazing number!`);
// }else if(favourite === 7){
//     console.log(`${favourite} is also an cool number`)
// }else{
//     console.log('Number is neither 7 or 23')
// }
// if (favourite !== 23){
//     console.log('why not the 23')
// }









// 23 Javascript
// Basic Boolean Logic

// And operator 
// if A & B are true when both are true
// A & B false if any one of them is false 

//OR Opertaor
// any one can be true if any one A or B is true 
// it only false when both are false 











// 24 Javascript
// LOgical Operators

// const driversLicense = true; //A
// const hasGoodVision = true; //B

// console.log(driversLicense && hasGoodVision)// true

// const driversLicense = true; //A
// const hasGoodVision = false; //B

// console.log(driversLicense && hasGoodVision)// false


// const driversLicense = true; //A
// const hasGoodVision = true; //B

// console.log(driversLicense || hasGoodVision)// true

// const shouldDrive = driversLicense && hasGoodVision;

// if(shouldDrive){
//     console.log('Sarah is able to drive')
// }else {
//     console.log("Someone esle should drive ")
// }

// const isTired = true; // C
// console.log(driversLicense && hasGoodVision &&isTired)

// if(driversLicense && hasGoodVision && isTired){
//     console.log('Sarah is able to drive')
// }else {
//     console.log("Someone esle should drive ")
// }









// 25 Javascript

// const scoreDoplins = (96 + 108 + 89 ) / 3;
// const scorKoalas = (88 + 91+ 110 ) / 3;
// console.log(scoreDoplins , scorKoalas)


// if (scoreDoplins> scorKoalas){
//     console.log("Dolphins wins the trophy")
// }else if(scorKoalas> scoreDoplins) {
//     console.log("Koalas wins the trophy")
// }else if(scoreDoplins=== scorKoalas){
//     console.log("Both wins the trophy")
// }

//Bonus 1 & Bonus 2

// const scoreDoplins = (97 + 112 + 80 ) / 3;
// const scorKoalas = (109 + 95+ 50) / 3;
// console.log(scoreDoplins , scorKoalas)


// if (scoreDoplins> scorKoalas && scoreDoplins >= 100){
//     console.log("Dolphins wins the trophy")
// }else if(scorKoalas> scoreDoplins && scorKoalas>=100) {
//     console.log("Koalas wins the trophy")
// }else if(scoreDoplins=== scorKoalas && scoreDoplins>= 100 && scorKoalas >= 100){
//     console.log("Both wins the trophy")
// }else {
//      console.log("No one wins the tropphy")
// }







//26 Javascript
//Swtich Statements asically use to in equality purpose
// easier to use in comparision to if-else conditional 


// const day = 'thursday';

// switch(day){
//     case 'monday':// day=== monday
//         console.log('Plan course ');
//         break;
//     case 'tuesday':
//         console.log('Prepare theory videos ');
//         break;
//     case 'wednesday':
//     case 'thursday':
//         console.log( 'Write code examples ');
//         break;   
//     case 'friday':
//         console.log(' Record videos ');
//         break;   
//     case 'saturday':
//     case 'sunday':
//         console.log(' Enjoy the weekend ');
//         break;               
//     default:
//         console.log('Not a valid day!')       
// }






// 27 Javascript
//Statements and expression 

//3+4 -> expression
// statements that produce a value is known as expression

//statements
//statements is a bigger piece of code that does not produce value without itself
// if (23>22);{
//     const str = ' 23 is a bigger'
// }


// note in templete litrals we can only put expression not statements



// 28 Javascript
// the conditional operator( tenary operator)

// const age = 23;
// age >= 18 /* condition part*/ ? console.log('I like to drink coke'): // if part
// console.log('I like to drink water');// else part

// easier way to write 
// const drink = age >= 18 ? 'wine' : 'water';
// console.log(drink);


// const age = 23;
// console.log(`I like to drink ${age >= 18 ? 'wine' : 'water'}`)

// by using tenary operator we use it in template litrals . tenary operator are use same as if-else statements but in a easier way





// 29 Javascript

// codinf challenge using tenary operators


// test data 1
// const bill = 275;
// const tip = bill > 50 && bill < 300 ? bill*0.15 : bill*0.2;
// const total = bill + tip;
// console.log(`'The bill was ${bill}, the tip was ${tip}, and the total value ${total}'`)


// test data 2
// const bill = 40;
// const tip = bill > 50 && bill < 300 ? bill*0.15 : bill*0.2;
// const total = bill + tip;
// console.log(`'The bill was ${bill}, the tip was ${tip}, and the total value ${total}'`)


// test data 3
// const bill = 430;
// const tip = bill > 50 && bill < 300 ? bill*0.15 : bill*0.2;
// const total = bill + tip;
// console.log(`'The bill was ${bill}, the tip was ${tip}, and the total value ${total}'`)






