 /*Data Types 

 There are 7 Data types in Javascript
 1.Number data types 
 which are always floating point numbers 
 2.String data types 
 it is a sequence of characters 
 3.Boolean Data Types 
 logical type that can be true or false 


 4.Undefined Data types 
 variables whose  value is not yet defined 
 5.Null data types 
 Also means Empty values 
 6.Symbol (ES2015)
 values that are unoque and cannot be changed .
 7 Biglnt(ES2020)
 large integers than the numbers can hold */

// Javascript has a dynamic typing 
// this means when a value has been created we do not need to manually assign the data types it automatically assign the data types to that values .

// data types are assign to value not to the variables .
/*
Code commenting 
1. Single line commennt //
2. Multi line comment /* */


// Boolen dta types 
let javascriptIsfun = true;
console.log( javascriptIsfun);

console.log(typeof javascriptIsfun);
console.log(typeof true);
console.log(typeof 27);
console.log(typeof "Jonas");

// Undefined data types
let year ;
console.log(year);
console.log(typeof year);

//dynamic typing 
year = 2220;
console.log(year);
console.log(typeof year);


// error in javascript

console.log(typeof null);
// it says object it never fix because of legacy reasons 

