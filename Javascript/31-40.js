// 32 Javascrip
// 'use strict';
// Activating Strict mode
// strict mode are use to avoide accidental bugs , avoid errors .
// to write more secure code 
// forbid us to do certain things 
// make visiblke errors to developer to see in console which does not show up without strict mode.

// example
// let hasdriversLicense = false;
// const passTest = true ;

// if (passTest) hasdriversLicense = true ;
// if (hasdriversLicense) console.log('I can drive')

//strict mode helpls developer to idendify the error 
//it points out the error in the code in console.



// const interface = 'Audio';
// const private = 23;
// also helps when some group of reserve keywords are use then also it helps to find out the error while without strict mode it wont gives error.




//33 Javascript
// Functions
// 'use strict';

// a piece of code which we use over and over in the program.
// function also recieve data and return data as well
//defining the function
// // function logger() {
//     console.log("My Name is Amaan");
// }

// //calling / running / Invoking the fucntion
// logger();
// logger();
// logger();

// function fruitProcessor (apples , oranges){

//     const juice = `Juice with ${apples} apples and ${oranges} oranges.`;
//     return juice;

// }

// // fruitProcessor(5,0);
// const appleJuice = fruitProcessor(5 ,0);
// console.log(appleJuice);
// //or 
// // console.log(fruitProcessor(5,0));


// const appleOrangeJuice = fruitProcessor(2,4);
// console.log(appleOrangeJuice);







// //34 Javascript
// // Functions declarattion vs. expression
// 'use strict';

//Function declaration 

// const age1 = calcAge1(1991);
// console.log(age1);

// function calcAge1 (birthYear){
//     return 2037 - birthYear;
// }

// const age1 = calcAge1(1991);
// console.log(age1);



//Function Expression 
// const calcAge2 = function (birthYear){
//     return 2037 - birthYear;
// }

// const age2 = calcAge2(1991);
// console.log(age2);

// the main diffrence betwwen function declaratiopn and function expressionn is that in function declaration we can call the function before the functioin is defined while in function expression we cannot call the function before is defining .





// 35 Javascript 
// Arrow function
// 'use strict';

// const calcAge3 = birthYear => 2037 - birthYear;
// const age3 =  calcAge3(1991);
// console.log(age3);


// const yearsUnitRetirement = (birthYear,firstName) => {
//     const age = 2037 - birthYear;
//     const retirement = 65 - age;
//     // return  retirement;
//     return `${firstName} retires in ${retirement} years `;

// }

// console.log(yearsUnitRetirement(1991, 'Amaan'));
// console.log(yearsUnitRetirement(1990 , 'Jonas'));

// when inside a a function there are more than one parameter then inside function we should return statements

// arrow function  does not get diss linera function









// 36 Javascript 
// Function calling other functions
// 'use strict';

// const cutpieces = function(fruit){
//     return fruit*3;
// }


// function fruitProcessor (apples , oranges){
//     const applePieces = cutpieces(apples);
//     const orangePieces = cutpieces(oranges);

//     const juice = `Juice with ${applePieces} piece of apples and ${orangePieces} piece of oranges.`;
//     return juice;
// }
// console.log(fruitProcessor(2,3));




// 36 Javascript 
//Reviewing  Functions
// 'use strict';

// const calcAge = function (birthYear){
//     return 2037 - birthYear;
// }
// const yearsUnitRetirement = function(birthYear,firstName)  {
//     const age = calcAge(birthYear);
//     const retirement = 65 - age;

//     if (retirement>0){
//         return retirement;
//     }else{
//         return -1;
//     }

//     // return  retirement;
//     // return `${firstName} retires in ${retirement} years `;

// }
// console.log(yearsUnitRetirement(1991, 'Amaan'));
// console.log(yearsUnitRetirement(1970, 'Mike'));


// three different ways to write function 
//function declaration 
// function expression 
// arrow function 






// 37 Javascript
// 'use strict';

// const calcAverage = (score1,score2 ,score3) => {
//     let average = (score1 + score2 + score3)/3;
//     average = average.toFixed(2);
//     return average;
// }

// const avgDolphins = calcAverage(85,23,71);
// console.log(`Dolphins average score is ${avgDolphins}`)
// const avgKoalas = calcAverage(11,11,49);
// console.log(`Koalas average score is ${avgKoalas}`)


// const checkWinner = function (avgDolphins,avgKoalas) {
//     if (avgDolphins>=2*avgKoalas){
//         console.log(`Dolphins win(${avgDolphins} vs. ${avgKoalas})`);
//     }else if (avgKoalas>=2*avgDolphins){
//         console.log(`Koalas win(${avgKoalas} vs. ${avgDolphins})`);
//     }else {
//         console.log("No teams wins")
//     }

// }
// checkWinner(avgDolphins , avgKoalas);

//Javascript 


// 38 Javascript 
// Array

// const friend1 = 'mohan';
// const friend2 = 'steven';
// const friend3 = 'peter';

// // we use array 

// const friends = ['mohan','steven','peter'];
// console.log(friends);

// // we can also use array in this way too
// const years = new Array(1991,2000,3209,2020);
// console.log(years);

// // how to take out elements of the array 
//  console.log(friends[0]);
//  console.log(friends[1]);
//  console.log(friends[2]);

//  console.log(years[0]);

//  // how to take out the number of element of array
//  console.log(friends.length);
//  // how to take out the last elements of the array using length
//  console.log(friends[friends.length - 1]);

//  // how to change the elements of the array
//  friends[2]= 'jay';
//  console.log(friends);

//  // array is mutuable 



// // we can aslo use array  inside another array
// const firstName = 'jonas'
// const jonas = [firstName, 'Amaan', 2037-1991, 'teacher',friends];
// console.log(jonas);

// //excersise
// const calcAge = function(birthYear){
//     return 2047-birthYear;

// }
// const year = [1990, 1967,2000,2010,2018];


// 40 Javascript

// Basic Array Operation
// (Method)

// const friends = ["Michal", "Amaan","Mohan"];
// const newLength = friends.push("Jay");
// console.log(friends);
// console.log(newLength);

// push is a method oor we can say it is built in function which is used to add a element in the end of an array and also gives the length of the array.


// friends.unshift("john");
// console.log(friends);
// unshift methods is used to add elements inthe start of the array 


// remove the last elements of the array 
// friends.pop();// last element
// const popped = friends.pop();
// console.log(friends);
// console.log(popped);


// friends.shift(); // first element
// console.log(friends);


// //indexOf methods help us to know the index of the elements in the array .
// console.log(friends.indexOf("Mohan"));
// console.log(friends.indexOf("Bob"));


// //includes methos helps us to know the elemnent is present in the array or not dependening upon the presenece it the return the result in boolean form
// friends.push(23);
// console.log(friends.includes("Mohan"));
// console.log(friends.includes("Bob"));
// console.log(friends.includes(23));

// // use case for includes
// if (friends.includes("Mohan")) {
//     console.log("You have a friend called Steven");

// }
