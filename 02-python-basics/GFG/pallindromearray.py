# Your task is to complete this function
# Function should return True/False or 1/0
def PalinArray(arr ,n):
    # Code here
    # reverse will be initilized with 0 since for each itreation it have to be zero 
    # reverse =0 was made by kaif bhai
    reverse=0
    count=0
    for element in arr:
        num=element
        reverse=0
        while num!=0:
            value=num%10
            reverse=reverse*10+value
            num=num//10
            
        if reverse==element:
            count=count+1
        else:
            return 0
            
         
    if count==len(arr):
        return 1
        
   
#{ 
 # Driver Code Starts
# Driver Program
if __name__=='__main__':
    t=int(input())
    for i in range(t):
        n = int(input())
        arr = list(map(int, input().strip().split()))
        if PalinArray(arr, n):
            print(1)
        else:
            print(0)
# Contributed By: Harshit Sidhwa

