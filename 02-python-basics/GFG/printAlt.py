#User function Template for python3

# arr is the array
# n is the number of elements in array

def printAl(arr,n):
    # your code here
    index = 0
    for i in arr:
        if(index %2 == 0):
            print(i,end=' ')
        index+=1
    
# [1,2,3,4]
# -> 1 3

#{ 
 # Driver Code Starts
#Initial Template for Python 3

if __name__=="__main__":
    t=int(input())
    for i in range(t):
        n=int(input('Enter Number'))
        arr=list(map(int,input().split()))
        printAl(arr,n)
        print()
# } Driver Code Ends