"""
Determine whether the input side length can form a triangle
If yes, calculate the perimeter and area of ​​the triangle

Version: 0.1
Author: 骆昊
Date: 2018-02-28
"""
import math

x=float(input("x = "))
y=float(input("y = "))
z=float(input("z = "))

if x+y>z and y+z>x and z+x>y:

    peri=x+y+z
    print('Perimeter of a trianle: %f ' % (peri))

    p=(x+y+z)/2
    area=math.sqrt(p*(p-x)*(p-y)*(p-z))
    print ("Area of a triangle: %f" % (area))

else:
    print("Cannot be a triangle")

# import math

# a = float(input('a = '))
# b = float(input('b = '))
# c = float(input('c = '))
# if a + b > c and a + c > b and b + c > a:
#     print('perimeter: %f' % (a + b + c))
#     p = (a + b + c) / 2
#     area = math.sqrt(p * (p - a) * (p - b) * (p - c))
#     print('area: %f' % (area))
# else:
#     print('cannot form a triangle')
