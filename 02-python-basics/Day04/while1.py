"""
用while循环实现1~100求和

Version: 0.1
Author: 骆昊
Date: 2018-03-01
"""
# initialize counter and sum
sum = 0
num = 1
while num <= 10:
    sum = sum + num
    num = num + 1
print(sum)
print(num)

# Sigma 1 to 10