"""
Enter two positive integers to calculate the greatest common divisor and the least common multiple

Version: 0.1
Author: 骆昊
Date: 2018-03-01
"""

x = int(input('x = '))
y = int(input('y = '))
if x > y:
    (x, y) = (y, x)
for factor in range(x, 0, -1):
    if x % factor == 0 and y % factor == 0:
        print('%dand%The greatest common divisor of d is%d' % (x, y, factor))
        print('%dand%The least common multiple of d is%d' % (x, y, x * y // factor))
        break
