"""
用while循环实现1~100之间的偶数求和

Version: 0.1
Author: 骆昊
Date: 2018-03-01
"""

sum, num = 0, 2
while num <= 100:
    sum = sum + num
    num = num + 2
print(sum)

#sigma of even numbers 2 to 100
