"""
格式化输出

Version: 0.1
Author: 骆昊
Date: 2018-02-27
"""

a = int(input('a = '))   #20                         
b = int(input('b = '))   #10

print('%d  + %d = %d' % (a, b, a + b))

print('%d - %d = %d' % (a, b, a - b))
print('%d * %d = %d' % (a, b, a * b))
print('%d / %d = %f' % (a, b, a / b))
print('%d // %d = %d' % (a, b, a // b))
print('%d %% %d = %d' % (a, b, a % b))
print('%d ** %d = %d' % (a, b, a ** b))


# Todo Amaan->done