"""
检查变量的类型

Version: 0.1
Author: 骆昊
Date: 2018-02-27
"""

a = 100
#int type
b = 1000000000000000000
#int type
c = 12.345
#float type
d = 1 + 5j
#complex type
e = 'A'
#str type
f = 'hello, world'
#str type
g = True
#boolean type


print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(f))
print(type(g))

# Todo Amaan->done