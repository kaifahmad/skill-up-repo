# Leap year --> A leap year is exactly divisible by 4 except for century years (years ending with 00). 
# The century year is a leap year only if it is perfectly divisible by 400.
# 2000
year = int(input('Enter year: '))
flag = False
if(year%400 == 0):
    flag=True
elif(year%4 == 0 and year%100 != 0):
    flag=True
else:
    flag =False

if(flag):
    print('Leap')
else:
    print('Not Leap')


# # 如果代码太长写成一行不便于阅读 可以使用\或()折行
# is_leap = (year % 4 == 0 and year % 100 != 0 or
#            year % 400 == 0)
