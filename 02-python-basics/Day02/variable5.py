"""
类型转换

Version: 0.1
Author: 骆昊
Date: 2018-02-27
"""

a = 100
b = str(a)
c = 12.345
d = str(c)
e = '123'
f = int(e)
g = '123.456'
h = float(g)
i = False
j = str(i)
k = ''
print("type of empty string",bool(k))
m = bool(k)


print(a)
#100
print(type(a))
#int type

print(b)
#100

print(type(b))
#str type
print(c)
#12.345
print(type(c))
#float type
print(d)
#12.345
print(type(d))
#str type
print(e)
#123
print(type(e))
#str type
print(f)
#123
print(type(f))
#int type
print(g)
#123.456
print(type(g))
#str type   
print(h)
#123.456
print(type(h))
#float type
print(i)
#false
print(type(i))
#bool type  
print(j)
#false
print(type(j))
#str type
print(k)
#hello
print(type(k))
#str type
print(m)
 
print(type(m))
#bool type

# Todo