"""
将input函数输入的数据保存在变量中并进行操作

Version: 0.1
Author: 骆昊
Date: 2018-02-27
"""

a = int(input('a = '))  #a=20
b = int(input('b = '))   #b=10

print(a + b)
#30
print(a - b)
#10
print(a * b)
#200
print(a / b)
#2.0
print(a // b)
#2
print(a % b)
#0
print(a ** b)
#20 raise to the power 10

# Todo