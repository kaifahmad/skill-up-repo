n = input('Enter n')
for i in range(0, n):
    # Do Something
    print(i)
# O(n)
"""
O(1) -> It takes approx. constant time to execute
t
n*t

Time Complexity
Time complexity is defined as the amount of time taken by an algorithm to run,
as a function of the length of the input. It measures the time taken to execute
each statement of code in an algorithm. It is not going to examine the total 
execution time of an algorithm.
"""
