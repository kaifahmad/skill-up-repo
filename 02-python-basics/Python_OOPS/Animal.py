class Animal:
    no_of_legs:int = 4
    sound:str
    def __init__(self,name="",legs=4,sound=" ") -> None:
        self.no_of_legs:int=legs
        self.sound:str=sound
        self.name:str=name
    # Attribute: features of a class
    @classmethod
    def makeSound(self):
        print(self.sound)

    def returnLegCount(self)->int:
        return self.no_of_legs
        
      
    

